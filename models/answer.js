import mongodb from 'mongodb';

let {ObjectID} = mongodb;

export default ({ Quizs, Answers }) => {
    const methods = {};

    methods.getAllUserScores = (userId) => Answers
                                            .aggregate([
                                                {
                                                    $match: {
                                                        userId: userId
                                                    }
                                                },
                                                {
                                                    $addFields: {
                                                        quizIdnew: { $toObjectId: "$quizId" }
                                                    }
                                                },
                                                {
                                                    $lookup: {
                                                        from: "Quizs",
                                                        localField: "quizIdnew",
                                                        foreignField: "_id",
                                                        as: "quizs"
                                                    }
                                                }
                                            ])
                                            .toArray()
                                            .then(answers => answers.map(answer => ({
                                                quizId: answer.quizId,
                                                score: answer.score, 
                                                quiz: { Id: answer.quizs[0]._id, ...answer.quizs[0]}
                                            })));

   
    methods.getUserScoreInQuiz = (userId, quizId) => Answers.aggregate([
                                                        {
                                                            $match: {
                                                                userId: userId,
                                                                quizId: quizId
                                                            }
                                                        },
                                                        {
                                                            $addFields: {
                                                                quizIdnew: { $toObjectId: "$quizId" }
                                                            }
                                                        },
                                                        {
                                                            $lookup: {
                                                                from: "Quizs",
                                                                localField: "quizIdnew",
                                                                foreignField: "_id",
                                                                as: "quizs"
                                                            }
                                                        }
                                                    ])
                                                    .toArray()
                                                    .then(answers => answers.length > 0 ? {
                                                            quizId: answers[0].quizId,
                                                            score: answers[0].score, 
                                                            quiz: { Id: answers[0].quizs[0]._id, ...answers[0].quizs[0]}
                                                        } : {});

    methods.add = async (quizId, answer) => {
        const quiz = await Quizs.findOne({_id: new ObjectID(quizId)});
        answer.score = answer.answers.reduce((acc, answer, index) => acc += answer == quiz.questions[index].answer ? quiz.questions[index].points : 0, 0);
        answer.quizId = quizId;
        const result = await Answers.insertOne(answer);
        return {
            Id: result.insertedId,
            ...answer
        }
    }
    return methods;
};
