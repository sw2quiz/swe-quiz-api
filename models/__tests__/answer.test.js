// import mongodb from 'mongodb';

// let {MongoClient} = mongodb;

// import buildAnswer from '../answer.js';

// describe('insert', () => {
//   let connection;
//   let db;

//   beforeAll(async () => {
//     connection = await MongoClient.connect(global.__MONGO_URI__, {
//       useNewUrlParser: true,
//     });
//     db = await connection.db(global.__MONGO_DB_NAME__);
//   });

//   afterAll(async () => {
//     await connection.close();
//     await db.close();
//   });

//   it('should insert a doc into collection', async () => {
//     const collections = {
//         Quizs: db.collection('Quizs'),
//         Answers: db.collection('Answers'),
//     };
//     const Answer = buildAnswer(collections)

//     const result = await Answer.getAllUserScores('1');
//     expect(result).toEqual([]);
//   });
// });

test('adds 1 + 2 to equal 3', () => {
  expect(1 + 2).toBe(3);
});