import mongodb from 'mongodb';

let {ObjectID} = mongodb;
  
  
export default ({ Quizs, Answers }) => {
    const methods = {};

    methods.all = () => Quizs.find().toArray().then((quizs) => quizs.map(quiz => ({ Id: quiz._id, ...quiz }) ));

    methods.get = (quizId) => Quizs.findOne({_id: new ObjectID(quizId)}).then(quiz => quiz ? { Id: quiz._id, ...quiz } : {} );

    methods.add = async (quiz) => {
        const result = await Quizs.insertOne(quiz);
        return {
            Id: result.insertedId,
            ...quiz
        }
    }

    methods.delete = (quizId) => Quizs.deleteOne({_id: new ObjectID(quizId)}).then(() => quizId);

    return methods;
};
  